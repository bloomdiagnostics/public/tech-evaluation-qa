Installation:
Java
Maven
Chrome/Firefox

To run :
mvn clean install 

1. Login to the website
2. Select women section
3. Click on faded short sleeves t-shirt
4. Click on add to cart button
5. Click on proceed to checkout button
6. Click on proceed to checkout button- to address page
7. Click on proceed to checkout button- to shipping page
8. Accept terms and conditions 
9. Click on proceed to checkout button
10. Click on proceed to checkout button to navigate to payments page
11. Click on pay bank by wire
12. Confirm the order
13. Verify the order confirmation


Task: 
1. Implement clicking on faded short sleeves t-shirt(step 3)
2. Confirm the order(step 11)
3. Verify the order confirmation(step 12)



