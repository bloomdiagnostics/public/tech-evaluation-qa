package com.bloom.challenge;

import java.io.IOException;
import java.util.HashMap;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;



public class BaseTest extends BrowserFactory {

	String homePageUrl = "http://automationpractice.com/index.php";
	public HashMap<String, String> data = new HashMap<>();

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser) throws Exception {
		getInstance();
		setDriver(browser);
		getDriver().manage().window().maximize();
		getDriver().get(homePageUrl);
	}

	@AfterMethod
	public void tearDown(ITestResult iTestResult) throws IOException {
		getDriver().quit();
	}
}
