
package com.bloom.challenge;

import java.lang.reflect.Method;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CheckoutPage;
import pages.HomePage;
import pages.LoginPage;

import utilities.Utils;

public class WebTest extends BaseTest {
	LoginPage login;
	HomePage home;
	CheckoutPage checkOut;

	@DataProvider
	public Object[][] getTestData() {
		Utils util = new Utils();
		Object data[][] = util.getData("Test");
		return data;
	}

	@Test(dataProvider = "getTestData")
	public void checkoutTest(Method method,String loginMail, String loginPwd) throws Exception {		
		login = new LoginPage();
		home = new HomePage();
		checkOut = new CheckoutPage();
		login.login(loginMail, loginPwd)	;
		home.selectWomenSection();
//		implement the method
		home.selectProduct();
		home.addToCart();
		home.proceedToCheckout();
		checkOut.proceedToAddressTab();
		checkOut.proceedToShippingTab();
		checkOut.acceptTnC();
		checkOut.proceedToCheckoutToPaymentsPage();
		checkOut.payByBankWire();		
	}
}
