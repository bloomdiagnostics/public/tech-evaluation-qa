package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import com.bloom.challenge.BaseTest;
import utilities.Utils;

public class CheckoutPage extends BaseTest {
	By women = By.linkText("Women");
	By tshirt = By.xpath("//a[@title='Faded Short Sleeve T-shirts']/ancestor::li");
	By submit = By.name("Submit");
	By checkOut = By.xpath("//*[@id='layer_cart']//a[@class and @title='Proceed to checkout']");
	By address = By.name("processAddress");
	By cgv = By.id("uniform-cgv");
	By carrier = By.name("processCarrier");
	By bankwire = By.className("bankwire");
	By nav = By.xpath("//*[@id='cart_navigation']/button");
	By heading = By.cssSelector("h1");
	By cart_navigation = By.xpath("//*[contains(@class,'cart_navigation')]/a[@title='Proceed to checkout']");
	By processAddress = By.name("processAddress");
	By confirmButton = By.xpath("//*[@id='cart_navigation']/button");	
	Utils util = new Utils();
	public void proceedToAddressTab() throws Exception {
		try {
			util.waitAndClick(cart_navigation);
		} catch (TimeoutException e) {			
			throw e;
		}
	}
	public void proceedToShippingTab() {
		try {			
			util.waitAndClick(processAddress);
		} catch (TimeoutException e) {		
			throw e;
		}
	}
	public void acceptTnC() throws Exception {
		try {
			util.waitAndClick(cgv);			
		} catch (TimeoutException e) {
			throw e;
		}
	}
	public void proceedToCheckoutToPaymentsPage() {
		util.waitAndClick(carrier);		
	}
	public void payByBankWire() {
		util.waitAndClick(bankwire);
	}
	public String confirmOrder() {
		util.waitAndClick(confirmButton);
		util.wait_explicit_till_element_Displayed(heading);
		return util.get_Element_Text(heading).toString();
	}
}
