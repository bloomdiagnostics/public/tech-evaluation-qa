package pages;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import com.bloom.challenge.BaseTest;
import utilities.Utils;


public class LoginPage extends BaseTest {

	Logger log= Logger.getLogger(LoginPage.class);
	By loginButton= By.className("login");
	By email= By.id("email");
	By password=By.id("passwd");
	By submitLogin=By.id("SubmitLogin");
	By heading= By.cssSelector("h1");
	Utils util ;
	public String login(String existingUserEmail, String existingUserPassword) throws Exception {
		util= new Utils();
		util.waitForPageLoad();
		try{		
		log.info("Waiting for Login button to be available");
		util.wait_explicit_till_element_Displayed(loginButton);
		util.click(loginButton);		
		log.info("Clicked Login button");
		util.wait_explicit_till_element_Displayed(email);
		log.info("Entering mail id as ::"+existingUserEmail);
		util.enterText(email, existingUserEmail);	
		util.wait_explicit_till_element_Displayed(password);
		if(existingUserPassword==null){
			log.info("The password seems to be blank.");
		}	
		util.enterText(password, existingUserPassword);
		util.wait_explicit_till_element_Displayed(submitLogin);
		util.click(submitLogin);
		util.waitForPageLoad(); 	
		log.info("Clicked submit button");
		util.wait_explicit_till_element_Displayed(heading);
		util.waitForPageLoad();
		return util.get_Element_Text(heading).toString();
		}catch(Exception e){
			log.error(e.toString());
			throw e;
		}
	}	
}
